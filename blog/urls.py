from django.urls import path
from . import views

urlpatterns = [
    path('',views.BlogListView.as_view(),name ='home'),
	path('post/detail/<int:pk>',views.BlogDetailView.as_view(),name ='Post_detail'),
	path('post/new/',views.BlogCreateView.as_view(),name ='Post_new'),
	path('post/edit/<int:pk>',views.BlogEditView.as_view(),name ='Post_edit'),
	path('post/delete/<int:pk>',views.BlogDeleteView.as_view(),name ='Post_delete'),
	

    ]
